# 基本概念复习

主要的基本概念和语法

* JavaScript 的基本概念与组成
* JavaScript 中的数据类型
* JavaScript 中的三种顺序结构(顺序, 选择, 循环)
* JavaScript 中的函数

