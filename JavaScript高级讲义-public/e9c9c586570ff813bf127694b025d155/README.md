# 面向对象进阶

介绍创建方法, 和使用技巧, 以及 继承

* 原型的基本概念
* 继承的概念
* Object 对象
* Fountion 对象
* 作用域链
* 闭包的基本概念
* getter 与 setter
