# 面向对象基础复习

主要内容:

* JavaScript 的构成以及 ECMAScript 与 JavaScript 的关系
* JavaScript 中的数据类型以及转换
* JavaScript 的语句
* JavaScript 中面向对象的基本概念
* 构造函数, 属性和方法的概念
* 常见内置对象
* 调试器的使用




