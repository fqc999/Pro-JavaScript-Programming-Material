# 面向对象基础

介绍基本语法, 基本对象的使用

* 面向对象的基本概念
* JavaScript 中的内置对象
* Array 与 Array 常用方法
* Date 与 Date 常用方法
* 包装对象
* 自定义对象
* 对象字面量
* Error 对象与异常处理
* Math 对象与全局对象
